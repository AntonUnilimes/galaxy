import * as THREE from "three";
import gParams from "./js/galaxyParametrs";
import Star from "./star";

export default class Viewer {
  constructor() {
    this.scene = new THREE.Scene();
    this.camera = new THREE.PerspectiveCamera(
      75,
      window.innerWidth / window.innerHeight,
      0.1,
      1000
    );
    this.renderer = new THREE.WebGLRenderer();

    this.Stars = new THREE.Object3D();

    this.onWindowResize = this.onWindowResize.bind(this);
  }

  init() {
    console.log("Viewer init", gParams);
    document.body.appendChild(this.renderer.domElement);
    this.renderer.setSize(window.innerWidth, window.innerHeight);
    this.camera.position.z = 5;
    this.cube = this.createCube();
    // this.scene.add(this.cube);
    this.createStars();
    this.setSpaceBG();
    window.addEventListener("resize", this.onWindowResize);
    this.animate();
  }

  createCube() {
    const geometry = new THREE.BoxGeometry();
    const material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
    const cube = new THREE.Mesh(geometry, material);
    return cube;
  }

  createStars() {
    const availableGRadius = gParams.gRadius - gParams.gCenterRadius;
    for (let index = 0; index < gParams.starsNumber; index++) {
      var angle = Math.random() * Math.PI * 2;
      let xCoord = Math.cos(angle) * availableGRadius;
      let yCoord = Math.sin(angle) * availableGRadius;

      const starPosition = {
        x: xCoord + Math.sign(xCoord) * gParams.gCenterRadius,
        y: gParams.gThickness * (Math.random() < 0.5 ? -1 : 1),
        z: yCoord + Math.sign(yCoord) * gParams.gCenterRadius,
      };
      // console.log("sff", starPosition);
      const starColor =
        gParams.starColors[
          Math.floor(Math.random() * gParams.starColors.length)
        ];

      const star = new Star(starPosition, starColor);
      this.scene.add(star.mesh);
    }
  }

  setSpaceBG() {
    var spacetex = THREE.ImageUtils.loadTexture("./img/space_bg.jpg");
    var spacesphereGeo = new THREE.SphereGeometry(20, 20, 20);
    var spacesphereMat = new THREE.MeshPhongMaterial();
    spacesphereMat.map = spacetex;
    this.spacesphere = new THREE.Mesh(spacesphereGeo, spacesphereMat);
    this.spacesphere.material.map.wrapS = THREE.RepeatWrapping;
    this.spacesphere.material.map.wrapT = THREE.RepeatWrapping;
    this.spacesphere.material.map.repeat.set(5, 3);

    this.scene.add(this.spacesphere);
    // var bgTexture = new THREE.TextureLoader().load("./img/space_bg.jpg");
    // bgTexture.minFilter = THREE.LinearFilter;
    // this.scene.background = bgTexture;
  }

  onWindowResize() {
    this.camera.aspect = window.innerWidth / window.innerHeight;
    this.camera.updateProjectionMatrix();

    this.renderer.setSize(window.innerWidth, window.innerHeight);
  }

  animate() {
    requestAnimationFrame(() => this.animate());

    this.cube.rotation.x += 0.01;
    this.cube.rotation.y += 0.01;

    this.renderer.render(this.scene, this.camera);
  }
}
