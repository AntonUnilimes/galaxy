import * as THREE from "three";
import Viewer from "./Viewer";

document.addEventListener("DOMContentLoaded", function (event) {
  this.viewer = new Viewer();
  this.viewer.init();
});
