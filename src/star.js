import * as THREE from "three";

class StarAnimation {
  constructor(animationConfig) {
    this.animationConfig = animationConfig;
    this.currentFrame = 0;
    this.delay = animationConfig.delay;
    this.isPaused = true;
    this.frameTimeout = null;
  }

  play(frame) {
    this.isPaused = false;
    this.currentFrame = frame || this.currentFrame;
  }

  pause() {
    this.isPaused = true;
  }

  stop() {
    this.isPaused = true;
    this.currentFrame = 0;
  }

  animate() {
    if (!this.isPaused) {
      if (this.currentFrame === 0) {
        this.animationConfig.onStart(0);
        this.animationConfig.animation(0);
        this.currentFrame++;
      } else if (this.currentFrame >= this.delay) {
        if (this.animationConfig.cycled) {
          this.currentFrame = 0;
          this.animationConfig.anmation(1);
        } else {
          this.animationConfig.animation(1);
          this.animationConfig.onEnd(1);
          this.stop();
        }
      } else {
        const currentAlpha = this.currentFrame / this.delay;
        this.animationConfig.animation(currentAlpha);
        this.currentFrame++;
      }
    }
  }
}

export default class Star {
  constructor(position, color, info = undefined) {
    this.position = new THREE.Vector3(position.x, position.y, position.z);
    this.color = new THREE.Color().setHex("0x" + color.replace("#", ""));
    this.colorHex = color;
    this.info = info;

    this.animationAlpha = 0;
    this.stepDelay = 0.005;

    this.nextAnimation = "animation";
    this.activeAnimation = null;
    this.animations = {};

    this.initMesh();
    this.initAnimations();
  }

  initMesh() {
    this.material = new THREE.MeshBasicMaterial({ color: this.color.clone() });
    this.geometry = new THREE.SphereGeometry(2, 15, 15);
    this.mesh = new THREE.Mesh(this.geometry, this.material);
    this.mesh.position.copy(this.position);
    this.mesh.userData.Star = this;
  }

  stopAnimation() {
    if (this.activeAnimation) {
      this.animations[this.activeAnimation].stop();
    }
    this.activeAnimation = null;
  }

  playAnimation() {
    if (!this.activeAnimation) {
      this.activeAnimation = "animation";
    }
    this.animations[this.activeAnimation].play();
  }

  setActiveAnimation(activeAnimation) {
    if (this.activeAnimation) {
      this.animations[this.activeAnimation].stop();
    }
    this.activeAnimation = activeAnimation || "animation";
    this.animations[this.activeAnimation].play();
  }

  initAnimations() {
    const context = this;
    const animation = new StarAnimation({
      name: "animation",
      context,
      animation: function (alpha) {
        const cycledAlpha = 1 + Math.sin(Math.PI * 2 * alpha) * 0.2;
        this.context.mesh.scale.set(cycledAlpha, cycledAlpha, cycledAlpha);
      },
      delay: 120,
      onStart: function () {
        this.context.mesh.scale.set(1, 1, 1);
        this.context.material.color.copy(this.context.color);
      },
      onEnd: function () {
        this.contex.mesh.scale.set(1, 1, 1);
        this.context.material.color.copy(this.context.color);
      },
      cycled: true,
    });
    this.animations["animation"] = animation;
  }

  animate() {
    if (this.activeAnimation) {
      this.animations[this.activeAnimation].animate();
    }
  }
}
