const bg = "#000000";

const gRadius = 100;
const gCenterRadius = 10;
const gThickness = 10;

const starColors = ["#ffd27d", "#ffa371", "#a6a8ff", "#fffa86", "#a87bff"];
const starsNumber = 100;

export default {
  bg,
  gRadius,
  gCenterRadius,
  gThickness,
  starColors,
  starsNumber,
};
